package team.pairfy.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;

@Configuration
public class CouchbaseConfig extends AbstractCouchbaseConfiguration {

    @Override
    public String getConnectionString() {
        return "couchbase://143.110.237.107";
    }

    @Override
    public String getUserName() {
        return "bugra";
    }

    @Override
    public String getPassword() {
        return "admin123";
    }

    @Override
    public String getBucketName() {
        return "product";
    }
}
