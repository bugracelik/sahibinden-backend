package team.pairfy.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SahibindenProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SahibindenProjectApplication.class, args);
    }

}
