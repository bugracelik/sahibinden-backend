package team.pairfy.app.repository;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;
import team.pairfy.app.domain.Product;

@Repository
public interface ProductRepository extends CouchbaseRepository<Product, String> {
}