package team.pairfy.app.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.sql.Time;

@Data
@Document
public class Product {
    @Id
    private Integer id;

    @Field
    @NotNull
    private String name;

    @Field
    @NotNull
    private Integer barcode;

    @Field
    @NotNull
    private Time addTime;

    @Field
    @NotNull
    private String producer;
}
