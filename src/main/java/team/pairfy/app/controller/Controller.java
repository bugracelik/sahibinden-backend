package team.pairfy.app.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import team.pairfy.app.domain.Product;
import team.pairfy.app.repository.ProductRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Time;

@RestController
public class Controller {

    @Autowired
    ProductRepository productRepository;

    @GetMapping("foo")
    public String foo() {
        return "foo";
    }

    @GetMapping("bar")
    public String bar(@RequestParam(value = "name", required = false, defaultValue = " ") String name) {
        return "merhaba " + name;
    }

    @GetMapping("tar")
    public void tar(HttpServletResponse response) throws IOException {
        response.sendRedirect("/zar");
    }

    @GetMapping("zar")
    public void zar() {
        throw new RuntimeException("patladın");
    }

    @GetMapping("add")
    public Product add() {
        Product product = new Product();
        product.setProducer("bugra");
        product.setName("bugra");
        product.setAddTime(new Time(100));
        product.setBarcode(213123);
        product.setId(11);
        return productRepository.save(product);
    }

}
